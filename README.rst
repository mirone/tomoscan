tomoscan
========

This library is offering an abstraction to access tomography data from various file format (.edf, .hdf5)

installation
''''''''''''

To install the latest 'tomoscan' pip package

.. code-block:: bash

    pip install tomoscan


You can also install tomoscanfrom source:

.. code-block:: bash

    pip install git+https://gitlab.esrf.fr/tomotools/tomoscan.git


documentation
'''''''''''''


General documentation can be found here: `https://tomotools.gitlab-pages.esrf.fr/tomoscan/ <https://tomotools.gitlab-pages.esrf.fr/tomoscan/>`_
