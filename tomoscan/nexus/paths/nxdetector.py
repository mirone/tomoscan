class NEXUS_DETECTOR_PATH:
    DATA = "data"

    IMAGE_KEY_CONTROL = "image_key_control"

    IMAGE_KEY = "image_key"

    X_PIXEL_SIZE = "x_pixel_size"

    Y_PIXEL_SIZE = "y_pixel_size"

    X_PIXEL_SIZE_MAGNIFIED = "x_magnified_pixel_size"

    Y_PIXEL_SIZE_MAGNIFIED = "y_magnified_pixel_size"

    MAGNIFICATION = "magnification"

    DISTANCE = "distance"

    FOV = "field_of_view"

    ESTIMATED_COR_FRM_MOTOR = "estimated_cor_from_motor"

    EXPOSURE_TIME = "count_time"

    X_FLIPPED = "x_flipped"

    Y_FLIPPED = "y_flipped"


class NEXUS_DETECTOR_PATH_V_1_0(NEXUS_DETECTOR_PATH):
    pass


class NEXUS_DETECTOR_PATH_V_1_1(NEXUS_DETECTOR_PATH):
    pass


class NEXUS_DETECTOR_PATH_V_1_2(NEXUS_DETECTOR_PATH_V_1_1):
    pass
