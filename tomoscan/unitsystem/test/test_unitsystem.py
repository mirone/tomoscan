# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2022 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "22/06/2021"


from tomoscan.unitsystem.energysystem import EnergySI
from tomoscan.unitsystem.timesystem import TimeSystem
from tomoscan.unitsystem.metricsystem import MetricSystem
from tomoscan.unitsystem.voltagesystem import VoltageSystem
import pytest


expected_energy_conversion = {
    "eV": EnergySI.ELECTRONVOLT,
    "keV": EnergySI.KILOELECTRONVOLT,
    "meV": EnergySI.MEGAELECTRONVOLT,
    "geV": EnergySI.GIGAELECTRONVOLT,
    "J": EnergySI.JOULE,
    "kJ": EnergySI.KILOJOULE,
}


@pytest.mark.parametrize(
    "energy_as_str, energy_as_si",
    (expected_energy_conversion.items()),
)
def test_energy_conversion(energy_as_str, energy_as_si):
    assert (
        EnergySI.from_str(energy_as_str) is energy_as_si
    ), f"check {energy_as_str} == {energy_as_si}"
    assert str(energy_as_si) == energy_as_str


def test_failing_energy_conversion():
    """Insure EnergySI.from_str raise a ValueErrror if cannot convert a string to an energy"""
    with pytest.raises(ValueError):
        EnergySI.from_str("toto")


def test_from_value():
    assert EnergySI.from_value(EnergySI.JOULE.value) is EnergySI.JOULE


expected_metric_conversion = {
    "m": MetricSystem.METER,
    "meter": MetricSystem.METER,
    "cm": MetricSystem.CENTIMETER,
    "mm": MetricSystem.MILLIMETER,
    "um": MetricSystem.MICROMETER,
    "nm": MetricSystem.NANOMETER,
}


@pytest.mark.parametrize(
    "metric_as_str, metric_as_si", expected_metric_conversion.items()
)
def test_metric_conversion(metric_as_str, metric_as_si):
    assert (
        MetricSystem.from_str(metric_as_str) is metric_as_si
    ), f"check {metric_as_str} == {metric_as_si}"
    assert str(metric_as_si) == str(
        MetricSystem.from_str(metric_as_str)
    ), f"check {metric_as_str} == {str(metric_as_si)}"


def test_failing_metric_conversion():
    """Insure MetricSystem.from_str raise a ValueErrror if cannot convert a string to an energy"""
    with pytest.raises(ValueError):
        MetricSystem.from_str("toto")


expected_time_conversion = {
    "second": TimeSystem.SECOND,
    "minute": TimeSystem.MINUTE,
    "hour": TimeSystem.HOUR,
    "day": TimeSystem.DAY,
    "millisecond": TimeSystem.MILLI_SECOND,
    "microsecond": TimeSystem.MICRO_SECOND,
    "nanosecond": TimeSystem.NANO_SECOND,
}


@pytest.mark.parametrize("time_as_str, time_as_si", expected_time_conversion.items())
def test_time_conversion(time_as_str, time_as_si):
    assert (
        TimeSystem.from_str(time_as_str) is time_as_si
    ), f"check {time_as_str} == {time_as_si}"
    assert str(time_as_si) == str(
        TimeSystem.from_str(time_as_str)
    ), f"check {time_as_str} == {str(time_as_si)}"


def test_failing_voltage_conversion():
    """Insure VoltageSystem.from_str raise a ValueErrror if cannot convert a string to an energy"""
    with pytest.raises(ValueError):
        VoltageSystem.from_str("toto")


expected_voltage_conversion = {
    "volt": VoltageSystem.VOLT,
}


@pytest.mark.parametrize("volt_as_str, volt_as_si", expected_voltage_conversion.items())
def test_voltage_conversion(volt_as_str, volt_as_si):
    assert (
        VoltageSystem.from_str(volt_as_str) is volt_as_si
    ), f"check {volt_as_str} == {volt_as_si}"
    assert str(volt_as_si) == str(
        VoltageSystem.from_str(volt_as_str)
    ), f"check {volt_as_str} == {str(volt_as_si)}"
