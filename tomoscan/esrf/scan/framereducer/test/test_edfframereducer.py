# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2022 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "06/01/2022"


from tomoscan.esrf.mock import MockEDF as _MockEDF
import numpy
import os
import fabio

from tomoscan.factory import Factory


class MockEDFWithRawRef(_MockEDF):
    def __init__(
        self,
        scan_path,
        n_radio,
        flats: dict,
        darks: dict,
        n_extra_radio=0,
        scan_range=360,
        n_recons=0,
        n_pag_recons=0,
        recons_vol=False,
        dim=200,
        scene="noise",
    ):

        self.flat_n = len(flats[list(flats.keys())[0]]) if len(flats) > 0 else 0
        self.dark_n = len(flats[list(darks.keys())[0]]) if len(darks) > 0 else 0
        self.flats = flats
        self.darks = darks
        self.dim = dim
        super().__init__(
            scan_path,
            n_radio,
            n_ini_radio=n_radio,
            n_extra_radio=n_extra_radio,
            scan_range=scan_range,
            n_recons=n_recons,
            n_pag_recons=n_pag_recons,
            recons_vol=recons_vol,
            dim=dim,
            scene=scene,
            dark_n=self.dark_n,
            flat_n=self.flat_n,
        )

        # add some raw dark and raw flat
        self.add_darks()
        self.add_flats()

        # add some darkHST and rehHST containing other data...
        self.addFalseReducedFlats()
        self.addFalseReducedDark()

    def add_dark(self, index):
        pass

    def add_flat(self, index):
        pass

    def add_flats(self):
        for i_serie, flats in self.flats.items():
            for i_flat, flat in enumerate(flats):
                file_name = f"ref{str(i_flat).zfill(4)}_{str(i_serie).zfill(4)}.edf"
                file_path = os.path.join(self.scan_path, file_name)
                if not os.path.exists(file_path):
                    edf_writer = fabio.edfimage.EdfImage(
                        data=flat,
                        header={
                            "motor_pos": f"{i_serie} 0.0 1.0 2.0",
                            "motor_mne": "srot sx sy sz",
                        },
                    )
                    edf_writer.write(file_path)

    def add_darks(self):
        for i_serie, darks in self.darks.items():
            assert darks.ndim == 3
            assert darks.shape[0] == 1
            file_name = "darkend{0:04d}.edf".format(i_serie)
            file_path = os.path.join(self.scan_path, file_name)
            if not os.path.exists(file_path):
                edf_writer = fabio.edfimage.EdfImage(
                    data=darks.reshape(darks.shape[1], darks.shape[2]),
                    header={
                        "motor_pos": f"{i_serie} 0.0 1.0 2.0",
                        "motor_mne": "srot sx sy sz",
                    },
                )
                edf_writer.write(file_path)

    def addFalseReducedDark(self):
        data = numpy.zeros((self.dim, self.dim)) - 1
        file_name = "dark.edf"
        file_path = os.path.join(self.scan_path, file_name)
        if not os.path.exists(file_path):
            edf_writer = fabio.edfimage.EdfImage(
                data=data,
                header={
                    "motor_pos": f"{-1} 0.0 1.0 2.0",
                    "motor_mne": "srot sx sy sz",
                },
            )
            edf_writer.write(file_path)

    def addFalseReducedFlats(self):
        for i_serie, flats in self.flats.items():
            data = numpy.zeros((self.dim, self.dim)) - 1
            file_name = "refHST{0:04d}.edf".format(i_serie)
            file_path = os.path.join(self.scan_path, file_name)
            if not os.path.exists(file_path):
                edf_writer = fabio.edfimage.EdfImage(
                    data=data,
                    header={
                        "motor_pos": f"{-1} 0.0 1.0 2.0",
                        "motor_mne": "srot sx sy sz",
                    },
                )
            edf_writer.write(file_path)


def test_reduce_edf(tmp_path):
    dim = 20
    folder_1 = tmp_path / "test1"
    folder_1.mkdir()
    n_proj = 12
    n_darks = 1
    # TODO: n darks are expected to be in accumulation. Created dark / flat should be normalized compared
    # to flat / dark
    raw_darks = numpy.ones((n_darks, dim, dim), dtype="f")
    raw_flats_s1 = numpy.asarray(
        [
            numpy.ones((dim, dim), dtype=numpy.float32),
            numpy.ones((dim, dim), dtype=numpy.float32) + 1.0,
            numpy.ones((dim, dim), dtype=numpy.float32) + 2.0,
            numpy.ones((dim, dim), dtype=numpy.float32) + 3.0,
            numpy.ones((dim, dim), dtype=numpy.float32) + 4.0,
        ]
    )
    raw_flats_s2 = numpy.asarray(
        [
            numpy.ones((dim, dim), dtype=numpy.float32) + 10.0,
            numpy.ones((dim, dim), dtype=numpy.float32) + 11.0,
            numpy.ones((dim, dim), dtype=numpy.float32) + 12.0,
            numpy.ones((dim, dim), dtype=numpy.float32) + 13.0,
            numpy.ones((dim, dim), dtype=numpy.float32) + 14.0,
        ]
    )

    MockEDFWithRawRef(
        scan_path=str(folder_1),
        n_radio=n_proj,
        flats={0: raw_flats_s1, 12: raw_flats_s2},
        darks={0: raw_darks},
    )
    scan = Factory.create_scan_object(str(folder_1))

    numpy.testing.assert_array_equal(
        scan.compute_reduced_darks(reduced_method="median")[0],
        scan.compute_reduced_darks(reduced_method="mean")[0],
    )

    numpy.testing.assert_array_equal(
        scan.compute_reduced_flats(reduced_method="median")[0],
        numpy.median(raw_flats_s1, axis=0).astype(numpy.int32),
    )
    numpy.testing.assert_array_equal(
        scan.compute_reduced_flats(reduced_method="mean", output_dtype=numpy.float32)[
            0
        ],
        numpy.mean(raw_flats_s1, axis=0),
    )

    numpy.testing.assert_array_equal(
        scan.compute_reduced_flats(reduced_method="median")[12],
        numpy.median(raw_flats_s2, axis=0).astype(numpy.int32),
    )
    numpy.testing.assert_array_equal(
        scan.compute_reduced_flats(reduced_method="mean")[12],
        numpy.mean(raw_flats_s2, axis=0).astype(numpy.int32),
    )

    numpy.testing.assert_array_equal(
        scan.compute_reduced_flats(reduced_method="first")[0],
        numpy.ones((dim, dim), dtype=numpy.int32),
    )

    numpy.testing.assert_array_equal(
        scan.compute_reduced_flats(reduced_method="last")[0],
        numpy.ones((dim, dim), dtype=numpy.int32) + 4,
    )

    numpy.testing.assert_array_equal(
        scan.compute_reduced_flats(reduced_method="first", output_dtype=None)[12],
        numpy.ones((dim, dim), dtype=numpy.float32) + 10.0,
    )


def test_reduce_edf_fails(tmp_path):
    folder_1 = tmp_path / "test2"

    MockEDFWithRawRef(scan_path=str(folder_1), n_radio=12, flats={}, darks={})
    scan = Factory.create_scan_object(str(folder_1))
    assert scan.compute_reduced_flats(reduced_method="first") == {}
    assert scan.compute_reduced_darks(reduced_method="last") == {}
