# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2022 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "04/01/2022"

from typing import Optional
from tomoscan.esrf.hdf5scan import ImageKey
from tomoscan.esrf.mock import MockHDF5 as _MockHDF5
import numpy


class MockHDF5(_MockHDF5):
    def __init__(
        self,
        scan_path,
        ini_dark: Optional[numpy.array],
        ini_flats: Optional[numpy.array],
        final_flats: Optional[numpy.array],
        dim: int,
        n_proj: int,
    ):
        assert ini_dark is None or ini_dark.ndim == 3, "ini_dark should be a 3d array"
        assert ini_flats is None or ini_flats.ndim == 3, "ini_dark should be a 3d array"
        assert (
            final_flats is None or final_flats.ndim == 3
        ), "ini_dark should be a 3d array"
        self._ini_darks = ini_dark
        self._ini_flats = ini_flats
        self._final_flats = final_flats
        super().__init__(
            scan_path=scan_path,
            dim=dim,
            create_ini_dark=ini_dark is not None,
            create_ini_flat=ini_flats is not None,
            create_final_flat=final_flats is not None,
            n_ini_proj=n_proj,
            n_proj=n_proj,
        )

    def add_initial_dark(self):
        for frame in self._ini_darks:
            self._append_frame(
                data_=frame.reshape(1, frame.shape[0], frame.shape[1]),
                rotation_angle=self.rotation_angle[-1],
                image_key=ImageKey.DARK_FIELD.value,
                image_key_control=ImageKey.DARK_FIELD.value,
                diode_data=None,
            )

    def add_initial_flat(self):
        for frame in self._ini_flats:
            self._append_frame(
                data_=frame.reshape(1, frame.shape[0], frame.shape[1]),
                rotation_angle=self.rotation_angle[-1],
                image_key=ImageKey.FLAT_FIELD.value,
                image_key_control=ImageKey.FLAT_FIELD.value,
                diode_data=None,
            )

    def add_final_flat(self):
        for frame in self._final_flats:
            self._append_frame(
                data_=frame.reshape(1, frame.shape[0], frame.shape[1]),
                rotation_angle=self.rotation_angle[-1],
                image_key=ImageKey.FLAT_FIELD.value,
                image_key_control=ImageKey.FLAT_FIELD.value,
                diode_data=None,
            )


def test_reduce_hdf5(tmp_path):
    """insure calculation of dark and flats are valid for a default use case"""
    dim = 20
    folder_1 = tmp_path / "test1"
    folder_1.mkdir()
    n_proj = 12
    n_darks = 10
    darks = numpy.ones((n_darks, dim, dim), dtype="f")
    flats_s1 = numpy.asarray(
        [
            numpy.zeros((dim, dim), dtype="f"),
            numpy.ones((dim, dim), dtype="f"),
            numpy.ones((dim, dim), dtype="f") + 1.0,
        ]
    )
    flats_s2 = numpy.asarray(
        [
            numpy.ones((dim, dim), dtype="f") + 10.0,
            numpy.ones((dim, dim), dtype="f") + 11.0,
            numpy.ones((dim, dim), dtype="f") + 12.0,
        ]
    )
    mock_scan = MockHDF5(
        scan_path=folder_1,
        ini_dark=darks,
        ini_flats=flats_s1,
        final_flats=flats_s2,
        dim=dim,
        n_proj=n_proj,
    )
    scan = mock_scan.scan
    numpy.testing.assert_array_equal(
        scan.compute_reduced_darks(reduced_method="median")[0],
        scan.compute_reduced_darks(reduced_method="mean")[0],
    )

    numpy.testing.assert_array_equal(
        scan.compute_reduced_flats(reduced_method="median")[n_darks],
        numpy.median(flats_s1, axis=0),
    )
    numpy.testing.assert_array_equal(
        scan.compute_reduced_flats(reduced_method="mean")[n_darks],
        numpy.mean(flats_s1, axis=0),
    )

    numpy.testing.assert_array_equal(
        scan.compute_reduced_flats(reduced_method="median")[n_darks + 3 + n_proj],
        numpy.median(flats_s2, axis=0),
    )
    numpy.testing.assert_array_equal(
        scan.compute_reduced_flats(reduced_method="mean")[n_darks + 3 + n_proj],
        numpy.mean(flats_s2, axis=0),
    )

    numpy.testing.assert_array_equal(
        scan.compute_reduced_flats(reduced_method="first", output_dtype=numpy.float64)[
            n_darks
        ],
        numpy.zeros((dim, dim), dtype=numpy.float64),
    )

    numpy.testing.assert_array_equal(
        scan.compute_reduced_flats(reduced_method="last", output_dtype=numpy.uint8)[
            n_darks
        ],
        numpy.ones((dim, dim), dtype=numpy.uint8) + 1,
    )

    numpy.testing.assert_array_equal(
        scan.compute_reduced_flats(reduced_method="first", output_dtype=numpy.int16)[
            n_darks + 3 + n_proj
        ],
        numpy.ones((dim, dim), dtype=numpy.int16) + 10.0,
    )


def test_reduce_hdf5_fails(tmp_path):
    folder_1 = tmp_path / "test2"
    mock_scan = MockHDF5(
        scan_path=folder_1,
        ini_dark=None,
        ini_flats=None,
        final_flats=None,
        dim=20,
        n_proj=12,
    )
    scan = mock_scan.scan
    assert scan.compute_reduced_flats(reduced_method="first") == {}
    assert scan.compute_reduced_darks(reduced_method="last") == {}
