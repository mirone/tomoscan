# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "13/12/2021"


from tomoscan.esrf.scan.utils import get_files_from_pattern
import os


def test_get_files_from_index_pattern(tmp_path):
    """
    Insure we can properly find files with the `index` pattern as
    myfile{index}.edf
    """
    test_dir = tmp_path / "test_dir"
    test_dir.mkdir()

    # create files to be tested
    indexes = (0, 10, 100, 1000, 2025)
    for index in indexes:
        open(os.path.join(test_dir, f"file{index}.EDF"), "w").close()
    # add some noise for test
    open(os.path.join(test_dir, "file.EDF"), "w").close()
    open(os.path.join(test_dir, "tata.edf"), "w").close()
    open(os.path.join(test_dir, "file3.hdf5"), "w").close()

    found_files = get_files_from_pattern(
        file_pattern="file{index}.edf",
        pattern="index",
        research_dir=str(test_dir),
    )

    assert len(found_files) == len(indexes)
    for index in indexes:
        assert index in found_files


def test_get_files_from_index_zfill4_pattern(tmp_path):
    """
    Insure we can properly find files with the `index` pattern as
    myfile{index_zfill4}.edf
    """
    test_dir = tmp_path / "test_dir"
    test_dir.mkdir()

    indexes = (0, 10, 100, 1000, 2025)
    for index in indexes:
        index_zfill4 = str(index).zfill(4)
        open(os.path.join(test_dir, f"file{index_zfill4}.edf"), "w").close()

    # add some noise for test
    open(os.path.join(test_dir, "file.EDF"), "w").close()
    open(os.path.join(test_dir, "tata0000.edf"), "w").close()
    open(os.path.join(test_dir, "file0003.hdf5"), "w").close()

    found_files = get_files_from_pattern(
        file_pattern="file{index}.edf",
        pattern="index",
        research_dir=str(test_dir),
    )

    assert len(found_files) == len(indexes)
    for index in indexes:
        assert index in found_files
