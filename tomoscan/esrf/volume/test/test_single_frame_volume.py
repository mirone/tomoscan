# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2022 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/
"""Test dedicated to the single frame volume: EDFVolume, TiffVolume, JP2KVolume"""

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "01/02/2022"


from glob import glob
import os
from tomoscan.esrf.identifier.folderidentifier import BaseFolderIdentifierMixIn
from tomoscan.esrf.volume.edfvolume import EDFVolume
from tomoscan.esrf.volume.jp2kvolume import JP2KVolume, has_glymur, has_minimal_opengl
from tomoscan.esrf.volume.tiffvolume import TIFFVolume, has_tifffile
from tomoscan.volumebase import VolumeBase
import numpy
import pytest
from tomoscan.identifier import VolumeIdentifier
from tomoscan.esrf.volume.mock import create_volume
from tomoscan.factory import Factory


_data = create_volume(
    frame_dims=(100, 100), z_size=11
)  # z_size need to be at least 10 to check loading from file name works
for i in range(len(_data)):
    _data[i] += 1
_data = _data.astype(numpy.uint16)


_metadata = {
    "nabu_config": {
        "dataset": {
            "location": "toto.hdf5",
            "entry": "entry0000",
        },
        "phase": {
            "method": "None",
            "padding_type": "edge",
        },
    },
    "processing_option": {
        "build_sino": {
            "axis_correction": "None",
            "enable_halftomo": True,
        },
        "flatfield": {
            "binning": "11",
            "do_flat_distortion": False,
        },
    },
}


volume_constructors = [
    EDFVolume,
]
if has_tifffile:
    volume_constructors.append(TIFFVolume)
if has_glymur and has_minimal_opengl:
    volume_constructors.append(JP2KVolume)


@pytest.mark.parametrize("volume_constructor", volume_constructors)
def test_create_volume_from_folder(tmp_path, volume_constructor):
    """Test a volume can be weel defined from it path"""
    acquisition_dir = tmp_path / "acquisition"
    os.makedirs(acquisition_dir)
    volume_dir = str(acquisition_dir / "volume")
    os.makedirs(volume_dir)
    volume = volume_constructor(folder=volume_dir, data=_data, metadata=_metadata)
    assert (
        len(
            glob(
                os.path.join(
                    volume_dir, f"*.{volume_constructor.DEFAULT_DATA_EXTENSION}"
                )
            )
        )
        == 0
    )
    assert (
        len(
            glob(
                os.path.join(
                    volume_dir, f"*.{volume_constructor.DEFAULT_METADATA_EXTENSION}"
                )
            )
        )
        == 0
    )
    assert tuple(volume.browse_data_files()) == tuple()
    assert tuple(volume.browse_data_urls()) == tuple()
    assert tuple(volume.browse_metadata_files()) == tuple()
    volume.save()
    assert (
        len(
            glob(
                os.path.join(
                    volume_dir, f"*.{volume_constructor.DEFAULT_DATA_EXTENSION}"
                )
            )
        )
        == _data.shape[0]
    )
    assert (
        len(
            glob(
                os.path.join(
                    volume_dir, f"*.{volume_constructor.DEFAULT_METADATA_EXTENSION}"
                )
            )
        )
        == 1
    )

    # check overwrite paramter
    with pytest.raises(OSError):
        volume.save()

    volume.overwrite = True
    volume.save()

    # check load data and metadata
    volume.clear_cache()
    volume.load()
    numpy.testing.assert_array_almost_equal(_data, volume.data)
    assert _metadata == volume.metadata

    # test get_slice function
    numpy.testing.assert_array_equal(
        volume.get_slice(xy=2),
        _data[2],
    )
    numpy.testing.assert_array_equal(
        volume.get_slice(xz=2),
        _data[:, 2, :],
    )
    numpy.testing.assert_array_equal(
        volume.get_slice(yz=2),
        _data[:, :, 2],
    )
    assert len(list(volume.browse_data_files())) == _data.shape[0]
    assert len(list(volume.browse_data_urls())) == _data.shape[0]
    assert len(list(volume.browse_metadata_files())) == 1


@pytest.mark.parametrize("volume_constructor", volume_constructors)
def test_several_writer(tmp_path, volume_constructor):
    """
    Test writing a volume from several instance of VolumeSingleFrameBase and reading the entire volume back
    """
    acquisition_dir = tmp_path / "acquisition"
    os.makedirs(acquisition_dir)
    volume_dir = acquisition_dir / "volume"
    os.makedirs(volume_dir)

    volume_1 = volume_constructor(
        folder=volume_dir,
        data=_data[:5],
        metadata=_metadata,
    )
    volume_2 = volume_constructor(folder=volume_dir, data=_data[5:], start_index=5)
    volume_1.save()
    volume_2.save()

    full_volume = volume_constructor(folder=volume_dir)
    full_volume.load()
    numpy.testing.assert_array_almost_equal(full_volume.data, _data)
    assert full_volume.metadata == _metadata


@pytest.mark.parametrize("volume_constructor", volume_constructors)
def test_volume_identifier(tmp_path, volume_constructor):
    """
    Insure each type of volume can provide an identifier and recover the data store from it
    """
    acquisition_dir = tmp_path / "acquisition"
    os.makedirs(acquisition_dir)
    volume_dir = acquisition_dir / "volume"
    os.makedirs(volume_dir)

    volume = volume_constructor(
        folder=volume_dir,
        data=_data,
        metadata=_metadata,
    )
    volume.save()
    identifier = volume.get_identifier()
    assert isinstance(identifier, VolumeIdentifier)

    del volume
    volume_loaded = volume_constructor.from_identifier(identifier)
    volume_loaded.load()
    numpy.testing.assert_array_almost_equal(volume_loaded.data, _data)
    assert volume_loaded.metadata == _metadata

    # check API Identifier.to() and identifier.from_str()
    identifier_from_str = identifier.to_str()
    duplicate_id = identifier.from_str(identifier_from_str)
    assert duplicate_id == identifier
    assert identifier_from_str == identifier.to_str()
    assert identifier != object()
    assert identifier != "toto"
    assert isinstance(identifier.short_description(), str)

    # check it can be reconstructed from the Factory
    assert isinstance(identifier, VolumeIdentifier)
    tomo_obj_from_str = Factory.create_tomo_object_from_identifier(
        identifier=identifier
    )
    assert isinstance(tomo_obj_from_str, type(volume_loaded))
    tomo_obj_from_identifier = Factory.create_tomo_object_from_identifier(
        identifier=identifier.to_str()
    )
    assert isinstance(tomo_obj_from_identifier, type(volume_loaded))

    # test hash
    hash(tomo_obj_from_str)
    hash(tomo_obj_from_str.get_identifier())


def test_folder_mix_in():
    """simpmle test of the BaseFolderIdentifierMixIn class"""

    class FolderIdentifierTest(BaseFolderIdentifierMixIn, VolumeIdentifier):
        pass

    class VolumeTest(VolumeBase):
        pass

    obj = FolderIdentifierTest(object=VolumeTest, folder="toto")
    with pytest.raises(NotImplementedError):
        obj.scheme


@pytest.mark.parametrize("volume_constructor", volume_constructors)
def test_volume_with_prefix(tmp_path, volume_constructor):
    """
    Test writing and reading volume with a file_prefix and using the identifier
    """
    acquisition_dir = tmp_path / "acquisition"
    os.makedirs(acquisition_dir)
    volume_dir = acquisition_dir / "volume"
    os.makedirs(volume_dir)
    file_prefix = "test"

    volume_1 = volume_constructor(
        folder=volume_dir,
        data=_data[:5],
        metadata=_metadata,
        volume_basename=file_prefix,
    )
    volume_1.save()

    full_volume = volume_constructor(folder=volume_dir, volume_basename=file_prefix)
    full_volume.load()
    numpy.testing.assert_array_almost_equal(full_volume.data, _data[:5])
    assert full_volume.metadata == _metadata

    obj_recreated = Factory.create_tomo_object_from_identifier(
        full_volume.get_identifier()
    )
    assert isinstance(obj_recreated, volume_constructor)
    obj_recreated.load()

    numpy.testing.assert_array_equal(
        obj_recreated.data,
        volume_1.data,
    )

    numpy.testing.assert_array_equal(
        obj_recreated.metadata,
        volume_1.metadata,
    )


@pytest.mark.parametrize("volume_constructor", volume_constructors)
def test_example(volume_constructor):
    """test static function 'example'"""
    assert isinstance(volume_constructor.example_defined_from_str_identifier(), str)
