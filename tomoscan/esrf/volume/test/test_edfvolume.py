# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2022 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "31/01/2022"


from datetime import datetime
import os
from tomoscan.esrf.volume.edfvolume import EDFVolume
import fabio
from tomoscan.esrf.volume.mock import create_volume
import numpy


_data = create_volume(
    frame_dims=(20, 20), z_size=11
)  # z_size need to be at least 10 to check loading from file name works
for i in range(len(_data)):
    _data[i] += 1


def test_edf_writer_header(tmp_path):
    """
    Test writing an edf volume from several EDFVolume and reading the entire volume back
    """
    acquisition_dir = tmp_path / "acquisition"
    os.makedirs(acquisition_dir)
    volume_dir = str(acquisition_dir / "volume")
    os.makedirs(volume_dir)

    sub_data = _data[:5]
    header = {"time": datetime.now().timestamp()}
    volume = EDFVolume(
        folder=volume_dir,
        data=sub_data,
        header=header,
    )
    volume.save()
    first_frame_path = os.path.join(volume_dir, "volume_0000.edf")
    assert os.path.exists(first_frame_path)
    assert "time" in fabio.open(first_frame_path).header

    # test get_slice function
    numpy.testing.assert_array_equal(
        volume.get_slice(xy=2),
        sub_data[2],
    )
    numpy.testing.assert_array_equal(
        volume.get_slice(xz=2),
        sub_data[:, 2, :],
    )
    numpy.testing.assert_array_equal(
        volume.get_slice(yz=2),
        sub_data[:, :, 2],
    )
    volume.data = None

    numpy.testing.assert_array_equal(
        volume.get_slice(xy=2),
        sub_data[2],
    )
    numpy.testing.assert_array_equal(
        volume.get_slice(xz=2),
        sub_data[:, 2, :],
    )
    numpy.testing.assert_array_equal(
        volume.get_slice(yz=2),
        sub_data[:, :, 2],
    )
