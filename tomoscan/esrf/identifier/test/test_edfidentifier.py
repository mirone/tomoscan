# coding: utf-8
# /*##########################################################################
# Copyright (C) 2016-2022 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
#############################################################################


__authors__ = ["H.Payno"]
__license__ = "MIT"
__date__ = "10/01/2022"


import os
from tomoscan.esrf.edfscan import EDFTomoScan
from tomoscan.esrf.hdf5scan import HDF5TomoScan
from tomoscan.esrf.mock import MockEDF
from tomoscan.identifier import ScanIdentifier
from tomoscan.factory import Factory
import pytest


def test_edf_identifier(tmp_path):
    """insure identifier is working for hdf5"""
    test_dir = tmp_path / "test_dir"
    os.makedirs(test_dir)

    scan = MockEDF.mockScan(
        scanID=str(test_dir),
        nRadio=12,
        dim=20,
    )
    identifier = scan.get_identifier()
    assert isinstance(identifier, ScanIdentifier)
    assert str(identifier).startswith("edf:")
    scan_from_identifier = EDFTomoScan.from_identifier(identifier)
    assert scan_from_identifier.path == scan.path
    scan_from_factory = Factory.create_tomo_object_from_identifier(
        identifier=str(identifier)
    )
    assert scan_from_factory.path == scan.path

    with pytest.raises(TypeError):
        HDF5TomoScan.from_identifier(identifier)

    # insure if TOMO_TYPE is not provided then it will still be considered as a scan
    identifier_as_str = str(identifier)
    identifier = ":".join(
        [identifier_as_str.split(":")[0], identifier_as_str.split(":")[-1]]
    )
    scan_from_factory = Factory.create_tomo_object_from_identifier(
        identifier=str(identifier)
    )
    assert scan_from_factory is not None
