Change Log
==========

0.9.0: 2022/06/19
-----------------

* EDFScan: add `dataset_basename` and `scan_info` to the constructor (PR 77)
    * `dataset_basename` is used to determine the projection files. If not provided fall back on folder name (as it was the case previously. Insure compatibility if not provided)
    * `scan_info`: users can provide .info file to be used to retrieve metadata. If not provided uses `datasaet_basename`.info. If not provided insure to have the same behavior as previously.

* add machine current (PR 78)
    * goes to `nexus_path.ELECTRIC_CURRENT_PATH` for HDF5 / nexus
    * goes to `nexus_path.ELECTRIC_CURRENT_PATH` for HDF5 / nexus

0.8.0: 2022/02/05
-----------------

* add nexus module: module dedicated to defines nexus path (PR 73)
* unitsystem
    * add timesystem. Module dedicated to time system (PR 72)
* improve management of a serie (PR 67)
* add the volume of concept inside tomoscan (PR 70)

0.7.0: 2022/01/04
-----------------

* esrf
    * add default methods to load and save darks / flats according to the file format (hdf5, edf): PR 65
    * improve mock class: PR 57
    * hdf5scan
        * add possibility to provide the nexus version on the constructor to insure backward compatibility: PR 58
    * add 'get_relative_file' helper.
* Miscellaneous
    * move documentation to autosummary: PR 56
    * replace some 'ref' by 'flat': PR 60

0.6.0: 2021/08/31
-----------------

* add validator: helper function to parse input file and look for some missing dataset, invalid values or broken link
* esrf
    * TomoScanBase
        * add a first version of the intensity normalization
        * add property `intensity_monitor` to read diode for example
        * add property `exposure_time` to get frame exposure time
    * add `Source` class
    * move _FOV to public
    * HDF5TomoScan
        * adapt to read from various version of nexus version
            * add `_NEXUS_PATHS` class to keep trace of the path through history and simplify reading from a specific version
        * `tomo_n` now return the value from `instrument/detector/tomo_n` instead of len(projections)
    * utils
        * get_compacted_dataslices: preserve defined scheme 
    * add `HDF5XRD3DScan` class to handle XRD-3D data

* Miscellaneous
    * move to setup.cfg

0.5.0: 2021/04/20
-----------------

* esrf
    * TomoScanBase
        * add `sequence_name`, `sample_name` and `group_size` properties
        * add `start_time` and `end_time` properties
        * add `x_translation`, `y_translation` and `z_translation` properties (Not handled by EDFTomoScan for now)
        * HDF5TomoScan
            * speed up sinogram generation
        EDFTomoscan
            * improve lazy loading
    * utils: add the concept of groups. A group for example is a set of sequences defining a zseries

0.4.0: 2020/11/09
-----------------

* esrf
    * HDF5TomoScan & EDFTomoScan add ignore_projections in the constructor
    * add properties to scanBase.TomoScanBase class:
        * alignment_projections
        * estimated_cor_frm_motor

    * add flat_field_correction function
    * move h5py dependency to 3.x

* Miscellaneous: add 'black' format

0.3.2: 2020/08/26
-----------------

* HDF5TomoScan
    * manage hdf5 files using the HDFFile Class in order to avoid any file corruption.

0.3.1: 2020/08/19
-----------------

* TomoScanBase
    * add field_of_view

* notebooks: rework notebooks

0.3.0: 2020/06/26
-----------------

* move default distance units to meter

* open with swmr mode by default

* utils:
    * add get_compacted_dataslices
    * rework HDF5 mock class


0.2.0: 2020/03/09
-----------------

* add some cache

* TomoScanBase
    * add distance
    * add energy

* EDFTomoScan
    * rework guess_index_frm_file_name

* HDF5TomoScan
    * add entry (one master file can have several sequence so several HDF5TomoScan)
    * add image_key (projection, dark...)
    * adapt to latest bliss file (bamboo)
    * add return_projs
    * add rotation_angle
    * add magnified pixel size
    * add frames

* rework UnitSystem


0.1.0: 2020/02/11
-----------------

* add tomoscan.esrf.edfscan

* add tomoscan.esrf.hdf5scan