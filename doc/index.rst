tomoscan
========

tomoscan aims to provide an unified interface to read tomography data from several file format.
For now it can handles raw .edf or .hdf5 (NXtomo) acquisitions.

.. note:: You can also convert EDF dataset to obtain an HDF5 - NXTomo compliant file by using `nxtomomill <https://gitlab.esrf.fr/tomotools/nxtomomill>`_.
          tomoscan will not produce any other file.

tomoscan is one of the `tomotools <https://gitlab.esrf.fr/tomotools>`_ developed at the `european synchrotron radiation facility <http://www.esrf.fr>`_

.. toctree::
   :maxdepth: 3

   api.rst
   tutorials/index.rst
